from sklearn.cluster import KMeans
import numpy as np

from sklearn.cluster import KMeans


def KmeansClusters(data):
    for i in range(10):
        X = data[["job", "education"]]
        kmeans = KMeans(n_clusters=i, random_state=0).fit(X)
        print(kmeans.cluster_centers_)
        data['color'] = kmeans.labels_
        sb.lmplot("job", "education", data=data, hue='color', fit_reg=False)
        plt.show()
