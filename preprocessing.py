import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sb
import numpy as np
import re
import nltk
from sklearn import preprocessing
from sklearn.feature_extraction.text import TfidfTransformer
from vaderSentiment.vaderSentiment import SentimentIntensityAnalyzer
from collections import defaultdict
from scipy import stats
#For labeling
d = defaultdict(preprocessing.LabelEncoder)

#For sentiment
analyzer = SentimentIntensityAnalyzer()

def cleanHtmlTags(raw_str):
  cleanr = re.compile(r'(<[^>]+>)|(<[^>]+\>)|(\n)|(\')|((http|https)://.* )')

  cleantext = re.sub(cleanr, '', raw_str)

  return cleantext

def cleanColumnTag(col):
    return col.apply(cleanHtmlTags)
    
def numberOfWords(col):
    return col.apply(lambda x : len(nltk.word_tokenize(x)))

def fillUnknown(col):
    return col.fillna("unknown")

def fullTextUser():
    return 

def analyzeSentiment(raw_txt):
    return analyzer.polarity_scores(raw_txt)

def visualiseAll(df):
    sb.heatmap(correlData.corr(method="kendall"), xticklabels=correlData.columns.values, yticklabels=correlData.columns.values)


def preprocessing(df):
    dataset = df
    dataset.fillna("unknown",inplace=True)
    label = dataset[["drinks","offspring","pets","smokes","drugs","diet","religion","sex","job","status","sign","orientation","education"]].apply(lambda x: d[x.name].fit_transform(x))
    labelFrame = pd.concat([label, dataset.loc[:, "essay0":"essay9"]], axis=1)
    
    #Merge essays 
    labelFrame["allEssays"] = labelFrame.loc[:, "essay0":"essay9"].apply(lambda x : ' '.join(x), axis=1)
    #Clean essays
    labelFrame["allEssays"] = cleanColumnTag(labelFrame["allEssays"]);
    #Get sentiment
    labelFrame["sentiment"] = labelFrame.allEssays.apply(analyzeSentiment) 
    #Get compound
    labelFrame["compound"] = labelFrame.sentiment.apply(lambda x : x['compound'])
    return labelFrame

# tokenizer
nltk.download('punkt')
from nltk import word_tokenize,sent_tokenize

def tokenize(raw_text):
    return nltk.word_tokenize(raw_text)

# Stop words
from nltk.corpus import stopwords
nltk.download('stopwords')
stopwords = stopwords.words('english')

def remove_stop_words(tokens):
    return  [i for i in tokens if not i in stopwords]

#Stemeer
from nltk.stem.porter import PorterStemmer
# Create p_stemmer of class PorterStemmer
p_stemmer = PorterStemmer()
def stem(tokens):
    return [p_stemmer.stem(i) for i in tokens]

import string

def remove_puncatation(stemmed_tokens):
    str_list = [''.join(c for c in s if c not in string.punctuation) for s in stemmed_tokens] # remove punctation
    return list(filter(None, str_list)) # remove empty strings
